package PoolFactoria;

import java.util.ArrayList;
import java.util.Iterator;

import P01.MyExceptionPool;

public class MyPoolG<T> {
	
	private ArrayList<T> m_lista;
	private ArrayList<Boolean> m_bool;
	private MyPoolFactoria<T> m_factoria;
	
	public MyPoolG(MyPoolFactoria factoria) {
		
		m_lista = new ArrayList<T>();
		m_bool = new ArrayList<Boolean>();
		m_factoria = factoria;
		
		for (int i = 0; i < 10; i++) {  
			
			T elemento = m_factoria.crear(); 
			m_lista.add(elemento); 
			m_bool.add(true);
		}
	} 
	
	
	
	public T get() throws MyExceptionPool {
		
		for (int i = 0; i < 10; i++) {
			if(m_bool.get(i)) {
				m_bool.set(i, false);
				return m_lista.get(i);
			}
			
		}
		throw new MyExceptionPool();
	}
	
	
	
	public void Return (T elemento) throws MyExceptionPool {
		for (int i = 0; i < 10; i++) {
			if(m_lista.get(i) == elemento && m_bool.get(i) == false) {
				m_bool.set(i, true);
				return; 
			}
		}
		throw new MyExceptionPool();
	}



	@Override
	public String toString() {
		String txt = "";
		for (int i = 0; i < 10; i++) {
			if(m_bool.get(i)) {
				txt += m_lista.get(i).toString() + " ";
			}
			
		}
		return txt;
		
		
	}
	
	
	
	
}
