package P01;

public class MyQueueTest {

	public static void main(String[] args) {
		
	MyQueue<Integer> pila = new MyQueue<Integer>();
		
		pila.Push(8);
		pila.Push(18);
		pila.Push(28);
		pila.Push(38);
		
		try {
			pila.Pop();
		} catch (MyExceptionListaVacia e) {
			
			System.out.println("La MyQueue esta vacia");
		}
		
		System.out.println(pila);
		
		pila.Push(48);
		pila.Push(58);
		
		try {
			pila.Pop();
		} catch (MyExceptionListaVacia e) {
			
			System.out.println("La MyQueue esta vacia");
		}
		
		System.out.println();
		System.out.println(pila);
		
	}

}
