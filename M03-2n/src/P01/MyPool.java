package P01;

import java.util.ArrayList;
import java.util.Iterator;


public class MyPool<T> {

	private  ArrayList<Integer> Piscina = new ArrayList<Integer>();
	private  ArrayList<Boolean> PiscinaBool = new ArrayList<Boolean>();

	

	public MyPool() {
		for (int i = 0; i < 10; i++) {
			Piscina.add(i);
			PiscinaBool.add(true);
		}
	}
	
	

	//
	
	public Integer get() throws MyExceptionPool {
		
		for (int i = 0; i < 10; i++) {
			if(PiscinaBool.get(i)) {
				PiscinaBool.set(i, false);
				return Piscina.get(i);
			}
		}
		
		throw new MyExceptionPool();
	
	}
	
	
	public void returnToPool(Integer element) throws MyExceptionPool {
		for (int i = 0; i < 10; i++) {
			
			if(PiscinaBool.get(i) == false && Piscina.get(i) == element) {
				PiscinaBool.set(i, true);
				return;
			}
		}
		throw new MyExceptionPool();
	}
	
	
	
	@Override
	public String toString() {
		String txt = "";
		for (int i = 0; i < 10; i++) {
			if(PiscinaBool.get(i)) {
				txt += Piscina.get(i).toString() + " ";
			}
			
		}
		return txt;
		
		
	}
	
	
	// public T create();
	
}
