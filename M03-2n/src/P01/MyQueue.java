package P01;

import java.util.ArrayList;

public class MyQueue<T> {

private ArrayList<T> cola;
	
	public MyQueue() {
		cola = new ArrayList<T>();
	}
	
	
	public T Pop() throws MyExceptionListaVacia {
		
		/*
		try {
			T holder = cola.get(0);
			cola.remove(0);
			return holder;
		}
		catch(Exception E) {
			System.out.println("Operacion invalida, MyQueue esta vacio");
			return null;
		}
		*/
		
		if(cola.size()<0)
			throw new MyExceptionListaVacia();
		
		else {
			T holder = cola.get(0);
			cola.remove(0);
			return holder;
		}
	}
	
	
	
	public void Push (T element) {
		
		cola.add(element);
		
	}
	
	@Override
	public String toString() {
		return "Contenido de la cola --> " + cola;
	}
	

}
