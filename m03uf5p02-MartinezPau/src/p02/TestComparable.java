package p02;

import java.util.ArrayList;
import java.util.Collections;

public class TestComparable {

	public static void main(String[] args) {
		 ArrayList<MyClassComparable> m_pilotosList = new ArrayList<MyClassComparable>();

		 MyClassComparable a = new MyClassComparable("Fernando", "Alpine", 14);
		 MyClassComparable b = new MyClassComparable("Max", "RedBull", 33);
		 MyClassComparable c = new MyClassComparable("Carlos", "Ferrari", 55);
		 MyClassComparable d = new MyClassComparable("Nikita", "Haas", 9);
		 MyClassComparable e = new MyClassComparable("Lando", "McLaren", 4);
		 MyClassComparable f = new MyClassComparable("Max", "AlphaTauri/ToroRosso", 33);
		 MyClassComparable g = new MyClassComparable("Carlos", "Ferrari", 54);
		 
		 
		 m_pilotosList.add(a);
		 m_pilotosList.add(b);
		 m_pilotosList.add(c);
		 m_pilotosList.add(d);
		 m_pilotosList.add(e);
		 m_pilotosList.add(f);
		 m_pilotosList.add(g);
		 
		 System.out.println(m_pilotosList);
		 
		 Collections.sort(m_pilotosList);
		 
		 System.out.println(m_pilotosList);
	}

}
