package p02;

import java.util.ArrayList;
import java.util.Iterator;


public class MyClassIterableStack<T> implements Iterable<T>{

	
	private ArrayList<T> pila;
	
	public MyClassIterableStack() {
		pila = new ArrayList<T>();
	}

	public T Pop() throws MyExceptionListaVacia {
		
		
		if(pila.size()<=0) {
			throw new MyExceptionListaVacia();
			//return null;
			
		}
		else {
			T holder = pila.get(pila.size()-1);
			pila.remove(pila.size()-1);
			return holder;
		}
		
	}
	
	public void Push (T element) {
		
	
	    pila.add(element);

	}

	
	@Override
	public String toString() {
		return "Contenido de la Pila --> " + pila;
	}
	
	
	
	/* ---------------------------------------------------------------------------- */
	
	@Override
	public Iterator<T> iterator() {
		
		return new MyIterable<T>();
	}
	
	
	private class MyIterable<T> implements Iterator<T>{

		private int contador = pila.size();
		
		@Override
		public boolean hasNext() {
			if(contador > 0)
				return true;
			
			return false;
		}

		@Override
		public T next() {
			contador--;
			return (T) pila.get(contador);
		}
		
			
	}

	
	
}
