package p02;

import java.util.ArrayList;
import java.util.Iterator;



public class TestIterableStack {
	/*
	public static void main(String[] args) {
		
		ArrayList<Object> list = new ArrayList<Object>();
		
		for(Object obj: list) {
			System.out.println(obj);
		}
		
		Iterator <Object> iterador = list.iterator();
		
		iterador.hasNext();
		iterador.next();
		
		while(iterador.hasNext()) {
			Object obj = iterador.next();
			System.out.println(obj);
		}
		
		
	}
	*/
	
	public static void main(String[] args) {
		
		MyClassIterableStack<Integer> a = new MyClassIterableStack<Integer>();
		
		
		for (int i = 0; i < 10; i++) {
			a.Push(i);
		}
		
		for (Integer num : a) {
			System.out.print(num);
		}

		
	}
	
	
}
