package p02;

public class MyClassComparable implements Comparable{

	
	String nombre;
	String equipo;
	int numero;
	
	public MyClassComparable(String nombre, String equipo, int numero) {
		this.nombre = nombre;
		this.equipo = equipo;
		this.numero = numero;
		
	}



	@Override
	public String toString() {
		return "[" + nombre +" " + equipo + " " + numero + "]";
	}

	@Override
	public int compareTo(Object a) {
		MyClassComparable piloto = (MyClassComparable) a;
		
		if(this.equals(piloto)) 
			return 0;
		
		else {
			if(this.nombre.compareTo(piloto.nombre) > 0) 
				return 1;
			
			else if(this.nombre.compareTo(piloto.nombre) < 0) 
				return -1;
			
			else {
				if(this.equipo.compareTo(piloto.equipo) > 0) 
					return 1;
				
				else if (this.equipo.compareTo(piloto.equipo) < 0) 
					return -1;
				
				else {
					if(this.numero > piloto.numero) 
						return 1;
					
					else 
						return -1;
				}
			}
		}
	}
	
	
	
	

}
