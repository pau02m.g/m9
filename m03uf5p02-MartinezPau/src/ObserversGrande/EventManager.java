package ObserversGrande;

import java.util.HashMap;
import java.util.Map;

public class EventManager {
	
	private static EventManager instance;
	
	public static EventManager getInstance() {
		if(instance == null) {
			instance = new EventManager();
		}
		return instance;
	}
	
	HashMap<String, Observable> Eventos; //= new HashMap<String, Observable>(); // string el nombre del evento, Observable pues el observable
	
	private EventManager() {
		Eventos = new HashMap<String, Observable>();
	}
	
	public void RegisterObserver(String EventName, Observer o) {
		if(Eventos.containsKey(EventName)) {
			Eventos.get(EventName).registerObserver(o);
		}
		else {
			Eventos.put(EventName, new Observable());
			Eventos.get(EventName).registerObserver(o);
		}
	}
	
	public void UnregisterObserver(String EventName, Observer o) {
		if(Eventos.containsKey(EventName)) {
			Eventos.get(EventName).unregisterObserver(o);
			System.out.println("Se a desregistrado el observer " +((Persona) o).getNombre() + " del evento " + EventName); //a
		
		}
		
	}
	
	public void NotifyObservers(String EventName) {
		if(Eventos.containsKey(EventName)) {
			Eventos.get(EventName).notifyObservers(EventName);
		}
		
	}
	
	
	
	public HashMap getEM() {
		return Eventos;
	}
	
	
	
	
}
