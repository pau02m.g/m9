package ObserversGrande;

public class TestEventManager {

	public static void main(String[] args) {
		
		Persona uno = new Persona("Yo");
		Persona dos = new Persona("Tekashi");
		Persona tres = new Persona("Anuel_AA");
		
		
		EventManager EM = EventManager.getInstance();
		
		
		EM.RegisterObserver("Renovar_DNI", uno);
		EM.RegisterObserver("Registrar_Material_Ludicodeportivo", uno);
		EM.RegisterObserver("Pasar_ITV", uno);
		
		EM.RegisterObserver("Registrar_Material_Ludicodeportivo", dos);
		
		EM.RegisterObserver("Pasar_ITV", tres);
		
		
		
		
		
		EM.NotifyObservers("Renovar_DNI");
		EM.NotifyObservers("Registrar_Material_Ludicodeportivo");
		EM.NotifyObservers("Pasar_ITV");


		
		//EM.UnregisterObserver("Renovar_DNI", uno);
		
		
	}

}
