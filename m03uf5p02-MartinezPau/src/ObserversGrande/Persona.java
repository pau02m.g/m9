package ObserversGrande;

public class Persona implements Observer{

	private String nombre;
	
	public Persona(String a) {
		this.nombre = a;
	}
	
	
	@Override
	public void NotifyObserver(String Event) {
		switch(Event)
		{
			case "Renovar_DNI":
				System.out.println(this.nombre + " acaba de renovar el DNI");
				break;
			case "Registrar_Material_Ludicodeportivo":
				System.out.println(this.nombre + " acaba de Registrar Material Ludicodeportivo");
				break;
			case "Pasar_ITV":
				System.out.println(this.nombre + " acaba de pasar la ITV");
				break;
				
			default:
				System.out.println("Evento: " + Event + " no existe");
				break;
		}
		
		
		
		
	}
	
	public String getNombre() {
		return this.nombre;
	}
}
