package ConsultaMedica;

public interface Observer{

	public void NotifyObserver(String nombre);
	
}
