package ConsultaMedica;

import java.util.ArrayList;
import java.util.Iterator;

public class Consulta extends Observable{

	ArrayList <Paciente> ListaPacientes;
	ArrayList <Paciente> ListaParaBorrar;
	
	
	public Consulta() {
		
		ListaPacientes = new ArrayList<Paciente>();
		ListaParaBorrar = new ArrayList<Paciente>();
	}
		

	
	
	public void SiguientePaciente(String nombre) {
		System.out.println("D: Se abre la consulta");
		notifyObservers(nombre);
		
		
		for (Paciente p : this.ListaParaBorrar) {
			this.unregisterObserver(p);
		}
		
	}




	public ArrayList<Paciente> getListaParaBorrar() {
		return ListaParaBorrar;
	}


}
