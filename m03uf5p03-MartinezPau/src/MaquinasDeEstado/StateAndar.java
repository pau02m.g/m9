package MaquinasDeEstado;

public class StateAndar extends State{

	@Override
	public void init() {
		System.out.println("Empiezo a andar");
		
	}

	@Override
	public void update() {
		System.out.println("Doy un paso");
		
	}

	@Override
	public void exit() {
		System.out.println("dejo de andar");
		
	}

}
