package MaquinasDeEstado;

public class StatePegar extends State{

	@Override
	public void init() {
		System.out.println("Empiezo a pegar");
		
	}

	@Override
	public void update() {
		System.out.println("Doy un golpe");
		
	}

	@Override
	public void exit() {
		System.out.println("dejo de pegar");
		
	}

}
