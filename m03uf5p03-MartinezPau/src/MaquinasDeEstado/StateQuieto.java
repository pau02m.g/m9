package MaquinasDeEstado;

public class StateQuieto extends State{

	@Override
	public void init() {
		System.out.println("Me quedo quieto");
		
	}

	@Override
	public void update() {
		System.out.println("Siguo quieto");
		
	}

	@Override
	public void exit() {
		System.out.println("dejo de estar quieto");
		
	}

}
