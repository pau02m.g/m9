package MaquinasDeEstado;

public abstract class State<T> {

	
	public abstract void init();
	
	public abstract void update();
	
	public abstract void exit();
	
	
}
