package MaquinasDeEstado;

import java.util.ArrayList;

import ComposicionCompleja.Component;

public class StateMachine {
	
	
	ArrayList<State> ListaEstados;
	private State EstadoActual;
	
	public StateMachine() {
		ListaEstados = new ArrayList<State>();
		
		
	}
	

	public void addState(State state) {
		ListaEstados.add(state);
	}
	
	public void removeState(State state) {
		ListaEstados.remove(state);
	}
	
	public <T extends State> void setState(Class<T> state) {
		for(State s : ListaEstados) {
			if(state.isInstance(s)) {
				this.EstadoActual = s;
			}
		}
	}
	
	
	public void update() {
		this.EstadoActual.update();
	}
	

}
