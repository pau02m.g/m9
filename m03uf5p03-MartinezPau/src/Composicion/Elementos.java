package Composicion;

import java.util.ArrayList;

public abstract class Elementos<T> {

	protected String nombre;
	
	public Elementos() {
		
	}

	@Override
	public String toString() {
		return "Tipo Elemento: " + this.nombre;
	}
	
	public abstract void utilizar();

}
