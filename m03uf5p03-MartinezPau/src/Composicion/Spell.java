package Composicion;

import java.util.ArrayList;

public class Spell {

	
	ArrayList<Elementos> lista_spell;
	private String nombre;
	
	public Spell(String s) {
		lista_spell = new ArrayList<Elementos>();
		this.nombre = s;
	}


	
	@Override
	public String toString() {
		return "Spell: " + this.nombre + " [" + lista_spell + "]";
	}
	
	public void utilizar() {
		System.out.println("uso el hechizo " + this.nombre);
		for(Elementos a: lista_spell) {
			a.utilizar();
		}
		System.out.println();
	}
	
	public void AnadirElemento(Elementos e) {
		lista_spell.add(e);
	}
	
}
