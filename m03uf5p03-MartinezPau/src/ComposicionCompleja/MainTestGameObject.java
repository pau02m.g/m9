package ComposicionCompleja;

public class MainTestGameObject {

	public static void main(String[] args) {
		
		
		GameObject go = new GameObject();

		
		
		go.addComponent(new JoinLine());
		go.addComponent(new RigidBody());
		
		go.update();
		
		go.removeComponent(JoinLine.class);
		
		go.update();
		
		
		
	}

}
