package ComposicionCompleja;

import java.util.ArrayList;

public class GameObject {
	
	//getComponent(RigidBody2D.class)
	
	ArrayList<Component> ListaComponentes;
	String nombre;
	
	public GameObject() {
		ListaComponentes = new ArrayList<Component>();
		this.nombre = "GameObject";
	}
	
	public GameObject(String a) {
		ListaComponentes = new ArrayList<Component>();
		this.nombre = a;
	}
	
	
	
	
	public void addComponent(Component element) {
		ListaComponentes.add(element);
	}
	
	public <T extends Component> void removeComponent(Class<T> element) {
		
		for(Component c : ListaComponentes) {
			if(element.isInstance(c)) {
				ListaComponentes.remove(c);
			}
		}
	}
	
	
	
	public <T extends Component> boolean hasComponent(Class<T> element) {
		for(Component c : ListaComponentes) {
			if(element.isInstance(c)) {
				return true;
			}
		}
		return false;
	}
	
	
	public <T extends Component> Class<T> getComponent(Class<T> element) {
		
		for(Component c : ListaComponentes) {
			if(element.isInstance(c)) {//element.isInstance(c) //c.getClass().equals(element.getClass())
				return element;
			}
		}
		return null;
	}
	
	
	public void update() {
		for(Component c : ListaComponentes) {
			c.updateComponent();
		}
	}
	
	


}
