package PackageColonia;

import java.util.ArrayList;



public class room {
	private static int id = 0;
	private int m_id;

	ArrayList<machine> lista_maquinas;
	private String nombre;
	
	
	
	public room(String s) {
		this.m_id = id;
		lista_maquinas = new ArrayList<machine>();
		this.nombre = s;
		id++;
	}
	
	public void accionSala() {
		System.out.println();
		System.out.println("--------------------");
		System.out.println("La sala: " + this.nombre + " va a realizar acciones");
		for(machine m : lista_maquinas) {
			m.utilizar();
		}
		System.out.println("--------------------");
		System.out.println();
	}
	
	

	public void ponerMaquina(machine m) {

		for (int i = 0; i < lista_maquinas.size(); i++) {
			if(lista_maquinas.get(i).getClass() == m.getClass()) {
				System.err.println("No se a podido a�adir la maquina\nYa hay otra del mismo tipo\n");
				return;
			}
		}
		
	 
		lista_maquinas.add(m);
		
	}
	
	public void quitarMaquina(machine m) {
		if(lista_maquinas.contains(m)) 
			lista_maquinas.remove(lista_maquinas.indexOf(m));
	}
	
	
	
	

	public int getId() {return this.m_id;}
	public void setId(int id) {this.m_id = id;}
	
	
	
	
	
	
	
	
	

	
}
