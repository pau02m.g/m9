package PackageColonia;

import java.util.ArrayList;

public class colony {

	ArrayList<room> Salas;
	
	public colony() {
		Salas = new ArrayList<room>();
	}
	
	public void afegirSala(room r) {
		Salas.add(r);
	}
	
	public void demolirSala(room r) {
		Salas.remove(Salas.indexOf(r));
	}
}
