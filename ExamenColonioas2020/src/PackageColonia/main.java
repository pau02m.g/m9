package PackageColonia;

import java.util.ArrayList;

public class main {

	public static void main(String[] args) {
		
		
		room Medbay = new room("Medbay");
		Medbay.ponerMaquina(new Dispenser());
		Medbay.ponerMaquina(new Autotreat());
		Medbay.ponerMaquina(new Laboratory());
		
		
		room Engineering = new room("Engineering");
		Engineering.ponerMaquina(new Dispenser());
		Engineering.ponerMaquina(new Laboratory());
		Engineering.ponerMaquina(new Welding());
		
		
		room Maintenance  = new room("Maintenance");
		Maintenance.ponerMaquina(new Dispenser());
		Maintenance.ponerMaquina(new Welding());
		Maintenance.ponerMaquina(new Broom());
		
	
		
		colony IslaDeLasTentaciones = new colony();
		IslaDeLasTentaciones.afegirSala(Medbay);
		IslaDeLasTentaciones.afegirSala(Engineering);
		IslaDeLasTentaciones.afegirSala(Maintenance);
		
		
		
		
		
		ArrayList<settler> Trabajadores = new ArrayList<settler>();
		for (int i = 0; i < 9; i++) {Trabajadores.add(new settler());}
		
		scheduler horario = scheduler.getInstance();
		

		horario.registrarTrabajador(Medbay, Trabajadores.get(0));
		horario.registrarTrabajador(Medbay, Trabajadores.get(1));
		horario.registrarTrabajador(Medbay, Trabajadores.get(2));
		
		horario.registrarTrabajador(Engineering, Trabajadores.get(3));
		horario.registrarTrabajador(Engineering, Trabajadores.get(4));
		horario.registrarTrabajador(Engineering, Trabajadores.get(5));
		
		horario.registrarTrabajador(Maintenance, Trabajadores.get(6));
		horario.registrarTrabajador(Maintenance, Trabajadores.get(7));
		horario.registrarTrabajador(Maintenance, Trabajadores.get(8));
		
		System.out.println("__________________________");
		System.out.println("|                         |");
		System.out.println("|         DIA 1           |");
		System.out.println("|_________________________|");
		System.out.println();

		horario.notificarTrabajadores(Maintenance);
		
		//horario.unregisterAllWorkers();
		
		for (int i = 0; i < 9; i++) {
			horario.desregistrarTrabajador(Trabajadores.get(i));
		}
		
		
		System.out.println("__________________________");
		System.out.println("|                         |");
		System.out.println("|         DIA 2           |");
		System.out.println("|_________________________|");
		System.out.println();
		
		horario.registrarTrabajador(Medbay, Trabajadores.get(6));
		horario.registrarTrabajador(Medbay, Trabajadores.get(7));
		horario.registrarTrabajador(Medbay, Trabajadores.get(8));
		
		horario.registrarTrabajador(Engineering, Trabajadores.get(3));
		horario.registrarTrabajador(Engineering, Trabajadores.get(4));
		horario.registrarTrabajador(Engineering, Trabajadores.get(5));
		
		horario.registrarTrabajador(Maintenance, Trabajadores.get(0));
		horario.registrarTrabajador(Maintenance, Trabajadores.get(1));
		horario.registrarTrabajador(Maintenance, Trabajadores.get(2));
		
		horario.notificarTrabajadores(Maintenance);
		
		
	}

}
