package PackageColonia;

public class settler implements Observer{

	private String nombre;
	private static int id = 0;
	private int id_sala;
	
	private room r;
	
	
	public settler() {
		this.nombre = "settler"+id;
		id++;
	}
	
	


	@Override
	public void NotifyObserver(int id_room) {
		if(this.r.getId() == id_room) {
			System.out.println(this.nombre + " va a trabajar en la sala " + id_room);
			r.accionSala();
		}
		else {
			
			System.out.println("El trabajador " + this.nombre + " anda --> ISRAEL B, C. TANGANA, LOWLIGHT - TRANQUILISIMO");
		}
	}
	
	
	
	
	
	
	public int getId_sala() {return id_sala;}
	public void setId_sala(int id_sala) {this.id_sala = id_sala;}
	public room getRoom() {return r;}
	public void setRoom(room r) {this.r = r;}
}
