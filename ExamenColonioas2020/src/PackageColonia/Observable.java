package PackageColonia;

import java.util.ArrayList;
import java.util.Iterator;

public class Observable {

	ArrayList<Observer> observers = new ArrayList<Observer>();
	
	

	public void registerObserver(Observer observer)
	{
		observers.add(observer);
	}
	
	public void unregisterObserver(Observer observer)
	{
		observers.remove(observer);
	}

	public void notifyObservers(int id_sala)
	{
		for(Observer observer: observers) {
			observer.NotifyObserver(id_sala);
		}
			
	}

}
