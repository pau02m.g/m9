package PackageColonia;

import java.util.ArrayList;

public class scheduler extends Observable{

	private static scheduler s; 
	
	private scheduler() {
		
	}
	
	public static scheduler getInstance() {
		if(s == null) {
			s = new scheduler();
		}
		return s;
	}
	
	

	public void registrarTrabajador(room r, settler s) {
		s.setRoom(r);
		s.setId_sala(r.getId());
		registerObserver(s);	
	}
	
	public void desregistrarTrabajador(settler s) {
		unregisterObserver(s);
	}
	
	
	public void notificarTrabajadores(room r) {
		
		notifyObservers(r.getId());
	}
	
	
	public void unregisterAllWorkers() {
		for(Observer s : observers) {
			unregisterObserver(s);
		}
	}
	
	
}
