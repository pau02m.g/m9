package Isla;

public interface Observer{

	public void NotifyObserver(Concursante a, Concursante b, int gravedad);
	
}
