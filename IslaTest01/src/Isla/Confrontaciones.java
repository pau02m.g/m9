package Isla;

public class Confrontaciones {
	
	boolean publica;
	Concursante a;
	Concursante b;
	int gravedad;
	

	public Confrontaciones(boolean publica, Concursante a, Concursante b, int gravedad) {
		this.publica = publica;
		this.a = a;
		this.b = b;
		this.gravedad = gravedad;
	}


	@Override
	public String toString() {
		return "Confrontaciones [publica=" + publica + ", a=" + a + ", b=" + b + ", gravedad=" + gravedad + "]";
	}
	
	

}
