package Isla;

import java.util.ArrayList;

public class Sala extends Observable{

	private ArrayList<Concursante> ConcursantesEnSala;
	private Camaras m_camara;
	
	public Sala(Camaras c) {
		ConcursantesEnSala = new ArrayList<Concursante>();
		this.m_camara = c;
		this.registerObserver(m_camara);
	}
	
	public void addConcursanteSala(Concursante c) {
		ConcursantesEnSala.add(c);
	}

	public ArrayList<Concursante> getConcursantesEnSala() {
		return ConcursantesEnSala;
	}

	public void setConcursantesEnSala(ArrayList<Concursante> concursantesEnSala) {
		ConcursantesEnSala = concursantesEnSala;
	}

	public Camaras getCamara() {
		return m_camara;
	}

	
}
