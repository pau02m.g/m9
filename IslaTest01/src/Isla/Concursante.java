package Isla;

public class Concursante{
	
	private String nombre;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "Concursante [nombre=" + nombre + "]";
	}
	
	
	
}