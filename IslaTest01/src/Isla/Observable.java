package Isla;

import java.util.ArrayList;
import java.util.Iterator;

public class Observable {

	ArrayList<Observer> observers = new ArrayList<Observer>();
	
	

	public void registerObserver(Observer observer)
	{
		observers.add(observer);
	}
	
	public void unregisterObserver(Observer observer)
	{
		observers.remove(observer);
	}

	public void notifyObservers(Concursante a, Concursante b, int gravedad)
	{
		
		for(Observer observer: observers) {
			observer.NotifyObserver(a, b, gravedad);
		}
			
	}

}
