package Isla;

import java.util.ArrayList;
import java.util.Random;

public class Camaras implements Observer{

	Random rand = new Random();
	
	private ArrayList<Confrontaciones> RegistroConforntaciones;
	public Camaras() {
		RegistroConforntaciones = new ArrayList<Confrontaciones>();
	}
	
	
	@Override
	public void NotifyObserver(Concursante a, Concursante b, int gravedad) {
		if(rand.nextBoolean()) {
			RegistroConforntaciones.add(new Confrontaciones(true, a, b, gravedad));
			//System.out.println("La camara a sido notificada");
			//System.out.println("Concursante 1: " + a.getNombre() + " Concursante 2: " + b.getNombre() + " / gravedad de la confrontacion: " + gravedad);
			
		}
		else {
			//System.out.println("La camara no a sido notificada");
		}
	}


	public ArrayList<Confrontaciones> getRegistroConforntaciones() {
		return RegistroConforntaciones;
	}


	public void setRegistroConforntaciones(ArrayList<Confrontaciones> registroConforntaciones) {
		RegistroConforntaciones = registroConforntaciones;
	}
	
	

}
