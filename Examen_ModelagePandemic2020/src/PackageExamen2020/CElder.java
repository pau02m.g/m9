package PackageExamen2020;

public class CElder extends Citizen{
	
	public CElder() {
		super(r.nextInt(41)+60);
	}

	@Override
	public boolean isSick() {
		return false;
	}
}
