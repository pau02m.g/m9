package PackageExamen2020;

import java.util.Random;



public abstract class Citizen implements Comparable{
	
	static Random r = new Random();
	
	private String nombre;
	private int edat;
	private boolean infectado;
	private static int id = 0;
	
	
	public Citizen() {
		
		this.nombre = "Citizen"+id;
		id++;
	}
	public Citizen(int i) {
		
		this.nombre = "Citizen"+id;
		this.edat = i;
		id++;
	}
	
	
	
	public String getNombre() {
		return nombre;
	}
	public int getEdat() {
		return edat;
	}
	public boolean isInfectado() {
		return infectado;
	}
	public void setInfectado(boolean i) {
		this.infectado = i;
	}
	
	public void infect() {
		this.infectado = true;
	}
	
	public void interact(Citizen citizen) {
		
		if(this.isInfectado()) {
			if(r.nextBoolean())
				citizen.infect();
		}
		else if(citizen.isInfectado()) {
			if(r.nextBoolean())
				this.infect();
		}
	}
	
	
	public abstract boolean isSick();
		
	

	@Override
	public String toString() {
		return "Citizen [nombre=" + nombre + ", edat=" + edat + "]";
	}
	
	
	@Override
	public int compareTo(Object o) {
		Citizen p = (Citizen)o;
		
		if(this.equals(p)) {
			return 0;
		}
		else {
			if(this.edat > p.getEdat()) {
				return 1;
			}
			else if(this.edat < p.getEdat()){
				return -1;
			}
			else {
				if(this.nombre.compareTo(p.getNombre()) > 0) {
					return 1;
				}
				else {
					return -1;
				}
			}
		}
		
		
		
		
	}
	
	
	
	
	

	
	
	
}
