package PackageExamen2020;

public class CAdult extends Citizen{
	public CAdult() {
		super(r.nextInt(41)+20);
	}

	@Override
	public boolean isSick() {
		if(this.isInfectado()) {
			return r.nextBoolean();
		}
		return false;
	}

}
