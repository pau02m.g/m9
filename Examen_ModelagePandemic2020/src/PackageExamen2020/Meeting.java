package PackageExamen2020;

import java.util.ArrayList;

public class Meeting {

	private ArrayList<Citizen> m_citizens;
	
	private Meeting() {

	}
	
	
	public Meeting(ArrayList<Citizen> citizens){
		m_citizens = citizens;
	}
	
	public void interact() {
		for(Citizen c : m_citizens) {
			int i = 1;
			for (; i < m_citizens.size(); i++) {
				c.interact(m_citizens.get(i));
			}
			i++;
		}
	}

}
