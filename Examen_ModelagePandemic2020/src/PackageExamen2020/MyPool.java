package PackageExamen2020;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class MyPool<T> {

	private ArrayList<Citizen> m_pool = new ArrayList<Citizen>();
	private ArrayList<Boolean> m_poolBool = new ArrayList<Boolean>();
	private Random r = new Random();
	
	
	public MyPool() {
		for (int i = 0; i < 18; i++) {

			if (i <= 5) {
				m_pool.add(new CYoung());
			} else if (i > 5 && i <= 11) {
				m_pool.add(new CAdult());
			} else {
				m_pool.add(new CElder());
			}
		}

		for (int i = 0; i < 2; i++) {
			int a = r.nextInt(18);
			m_pool.get(a).infect();
		}
		for (int i = 0; i < 18; i++) {
			m_poolBool.add(true);

		}
		
	}

	//

	public Citizen get() throws MyExceptionPool { //seria para meter en cuarentena

		for (int i = 0; i < 10; i++) {
			if (m_poolBool.get(i)) {
				m_poolBool.set(i, false);
				return m_pool.get(i);
			}
		}

		throw new MyExceptionPool();

	}

	public void returnToPool(Citizen element) throws MyExceptionPool {
		for (int i = 0; i < 10; i++) {

			if (m_poolBool.get(i) == false && m_pool.get(i) == element) {
				m_poolBool.set(i, true);
				return;
			}
		}
		throw new MyExceptionPool();
	}

	
	
	public void Cuarentena(Citizen c) throws MyExceptionPool {
		m_poolBool.set(m_pool.indexOf(c), false);
	}
	
	

	
	
	
	//noseque
	
	public ArrayList<Citizen> getList(){
		return m_pool;
	}



}
